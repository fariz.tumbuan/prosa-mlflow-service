#!/bin/bash

sudo docker run \
	-p 9000:9000 \
	-v /srv/data2/mlflow_server/_data/minio/data/:/data \
	--env-file minio_env.list \
	minio/minio \
	server /data
