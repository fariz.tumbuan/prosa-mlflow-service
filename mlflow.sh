#!/bin/bash
export MLFLOW_S3_ENDPOINT_URL=http://localhost:9000
export AWS_ACCESS_KEY_ID=accesskey
export AWS_SECRET_ACCESS_KEY=secretkey

mlflow server \
    --backend-store-uri postgresql:///mlruns \
    --default-artifact-root s3://mlruns \
    -h 10.181.131.13 \
    -p 9001
